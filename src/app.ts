// Création des classes
// Création d'une classe Customer
class Customer {
    //Propriétés définies directement dans le constructeur 
    // Ajout de la propriété Address
    customerAddress: Address | undefined;
    // Constructeur
    constructor(public customerID: number, public customerName: string, public customerEmail: string) {
        this.customerID = customerID;
        this.customerName = customerName;
        this.customerEmail = customerEmail;
    }
    // Méthode
    displayInfo(): string {
        return `Customer ID: ${this.customerID}, Name: ${this.customerName}, Email: ${this.customerEmail}, Address: ${this.customerAddress}`;
    }
    // Ajout des méthodes de renseignement de l'adresse et d'affichage d'adresse du client
    setAddress(customerAddress: Address): void {
        this.customerAddress = customerAddress;
    }
    displayAddress(): string {
        if (this.customerAddress) {
            return `Address: ${this.customerAddress.street}, ${this.customerAddress.postalCode} ${this.customerAddress.city}, ${this.customerAddress.country}`;
        } else {
            return "No address found";
        }
    }
}

//Création d'une classe Product
class Product {
    //Propriétés
    //ajout de la propriété dimension et modification du constructeur et de la méthode pour inclure le type Dimensions
    productDimensions: Dimensions;
    //Constructeur
    constructor(public productID: number, public productName: string, public productWeight: number, public productPrice: number, public dimensions: Dimensions) {
        this.productID = productID;
        this.productName = productName;
        this.productWeight = productWeight;
        this.productPrice = productPrice;
        this.productDimensions = dimensions;
    }
    // Méthode
    displayDetails(): string {
        return `Product ID: ${this.productID}, Name: ${this.productName}, Weight: ${this.productWeight}, Price: ${this.productPrice} €, Dimensions: ${this.productDimensions.length}x${this.productDimensions.width}x${this.productDimensions.height}`;
    }
}

// Création des types
// Type Dimensions
type Dimensions = {
    length: number;
    width: number;
    height: number;
}

// Type Address
type Address = {
    street: string;
    city: string;
    postalCode: string;
    country: string;
}

// Création des Enumérations
// Enum ClothingSize
enum ClothingSize {
    XS = "XS",
    S = "S",
    M = "M",
    L = "L",
    XL = "XL",
    XXL = "XXL",
}
// Enum ShoeSize avec précision de taille européenne parce que mettre 36=36 c'est bizarre et que mettre Shoe Size devant c'est gênant
enum ShoeSize {
    EU_36 = 36,
    EU_37 = 37,
    EU_38 = 38,
    EU_39 = 39,
    EU_40 = 40,
    EU_41 = 41,
    EU_42 = 42,
    EU_43 = 43,
    EU_44 = 44,
    EU_45 = 45,
    EU_46 = 46,
}

// Création des Héritages
class Clothing extends Product {
    clothingSize: ClothingSize;

    constructor(productID: number, productName: string, productWeight: number, productPrice: number, productDimensions: Dimensions, clothingSize: ClothingSize) {
        super(productID, productName, productWeight, productPrice, productDimensions)
        this.clothingSize = clothingSize;
    }
    displayDetails(): string {
        return `${super.displayDetails()}, Clothing Size: ${this.clothingSize}`;
    }
}


class Shoes extends Product {
    shoeSize: ShoeSize;

    constructor(productID: number, productName: string, productWeight: number, productPrice: number, productDimensions: Dimensions, shoeSize: ShoeSize) {
        super(productID, productName, productWeight, productPrice, productDimensions);
        this.shoeSize = shoeSize;
    }

    displayDetails(): string {
        return `${super.displayDetails()}, Shoe Size: ${this.shoeSize}`;
    }
}

// Création des Méthodes
    // Création de la classe Order
class Order {
    // Propriétés
    orderID: number;
    customer: Customer;
    productList: Product[] = []; // Initialisation de la liste avec un tableau vide pour ne pas initialiser un seul produit
    orderDate: Date;
    // Ajout de la propriété Delivery
    delivery: Deliverable | undefined;

    constructor(orderID: number, customer: Customer, productList: Product[], orderDate: Date) {
        this.orderID = orderID;
        this.customer = customer;
        this.productList = productList; //productList est bien un tableau de produits
        this.orderDate = orderDate;
    }
    // Méthode pour ajouter un produit à la commande
    addProduct(productList: Product): void {
        this.productList.push(productList);
    }
    // Méthode pour retirer un produit de la commande
    removeProduct(productID: number): void {
        this.productList = this.productList.filter((productList) => productList.productID !== productID);
    }
    // Méthode pour calculer le poids total de la commande
    calculateWeight(): number {
        let totalWeight = 0;
        this.productList.forEach((product) => {
            totalWeight += product.productWeight;
        });
        return totalWeight;
    }
    // Méthode pour calculer le prix total de la commande
    calculateTotal(): number {
        let totalCost = 0;
        this.productList.forEach((product) => {
            totalCost += product.productPrice;
        });
        return totalCost;
    }
    // Méthode pour afficher les détails de la commande: infos utilisateur, infos de chaque produit et total de la commande
    displayOrder(): string {
        const customerInfo = this.customer.displayInfo();
        const productInfo = this.productList.map((product) => product.displayDetails()).join("\n");
        const totalInfo = `Total Price: ${this.calculateTotal()} €`;

        return `Client: ${customerInfo}\n Products: ${productInfo}\n Total: ${totalInfo}`;
    }
    // Ajout de la méthode permettant d'assigner un service de livraison à la commande
    setDelivery(delivery: Deliverable): void {
        this.delivery = delivery;
    }
    // Ajout de la méthode permettant de calculer les frais de livraison en fonction du poids total de la commande en utilisant calculateShippingFee de Delivery
    calculateShippingCosts(): number | undefined {
        let shippingWeight = this.calculateWeight();
        if (this.delivery && shippingWeight > 0) {
            return this.delivery.calculateShippingFee(shippingWeight);
        } else {
            return undefined;
        }
    }
    // Ajout de la méthode permettant d'estimer le délai de livraison de la commande en utilisant la méthode estimateDeliveryTime
    estimateDeliveryTime(): number | undefined {
        let deliveryWeight = this.calculateTotal();
        if (this.delivery && deliveryWeight > 0) {
            return this.delivery.estimateDeliveryTime(deliveryWeight);
        } else {
            return undefined;
        }
    }
}

// Création des INterfaces
// Interface Deliverable
interface Deliverable {
    estimateDeliveryTime(weight: number): number;
    calculateShippingFee(weight: number): number;
}
// Création des classes StandardDelivery & ExpressDelivery implémentant Deliverable
class StandardDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        if (weight < 10) {
            return 7;
        } else {
            return 10;
        }
    }
    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 5;
        } else if (weight <= 5) {
            return 10;
        } else {
            return 20;
        }
    }
}

class ExpressDelivery implements Deliverable {
    estimateDeliveryTime(weight: number): number {
        if (weight <= 5) {
            return 1;
        } else {
            return 3;
        }
    }
    calculateShippingFee(weight: number): number {
        if (weight < 1) {
            return 8;
        } else if (weight <= 5) {
            return 14;
        } else {
            return 30;
        }
    }
}

// Exemple d'utilisation du code
// Création d'instances
const customerAddress: Address = {
    street: "1OO7 Mountain Drive",
    city: "Gotham City",
    postalCode: "12345",
    country: "DCComics"
};

const customer = new Customer(1, "Bruce Wayne", "bruce.wayne@batman.com");
customer.setAddress(customerAddress);

const productDimensions: Dimensions = {
    length: 10,
    width: 5,
    height: 2
};

const cape = new Clothing(1, "Bat Cape", 2.5, 50, productDimensions, ClothingSize.L);
const boots = new Shoes(2, "ComBat Boots", 3.2, 90, productDimensions, ShoeSize.EU_44);

const order = new Order(1, customer, [cape, boots], new Date());

const standardDelivery = new StandardDelivery();
order.setDelivery(standardDelivery);

// Ajout d'un produit supplémentaire à la commande
order.addProduct(new Clothing(3, "Tights", 0.8, 20, productDimensions, ClothingSize.L));

// Retrait du produit avec l'ID 2 de la commande
order.removeProduct(2);

// Affichage des détails de la commande, y compris les informations sur la livraison
console.log(order.displayOrder());

// Calcul des frais de livraison
const shippingCosts = order.calculateShippingCosts();
console.log(`Shipping Costs: ${shippingCosts !== undefined ? `${shippingCosts} €` : "Not available"}`);

// Estimation du délai de livraison
const deliveryTime = order.estimateDeliveryTime();
console.log(`Estimated Delivery Time: ${deliveryTime !== undefined ? `${deliveryTime} days` : "Not available"}`);
